#include "Rii.h"

void add_colors(hashtable t, u32 i, u32 j);

void add_colors(hashtable table, u32 i, u32 j) {
    u32 idx = lookuphash(table, i);
    if (idx == UINT32_MAX) {
        idx = inserttohash(table, i);
    }
    neighbor elem = table->array[idx]->value;
    elem->length++;
    u32 size = elem->length;
    u32 *vertexlist = elem->list;

    vertexlist = (u32*)realloc(vertexlist, size * sizeof(u32));
    vertexlist[size-1] = j;
    elem->list = vertexlist;

    return;
}

u32 Greedy(Grafo G){
    u32 colornum = 0;
    u32* colorlist = calloc(G->table->count, sizeof(u32));
    u32 listlength = G->table->count;
    u32 i, j, idx;
    _Bool encontreadj = 0;

    if (G->colorhash == NULL) {
        hashtable newhash = createhash(120);
        G->colorhash = newhash;
    }
    colorlist[0] = 0;
    for (i = 0; i < listlength; i++) {
        for (j = 0; j < i; j++) {
            if (sonadjacentes(G, i, j) == 0) {
                colorlist[i] = colorlist[j];
                encontreadj = 1;
                break;
            }
        }
        if (encontreadj == 0) { 
            colorlist[i] = i;
            colornum++;
        }
        add_colors(G->colorhash, colorlist[i], G->names[i]);
        idx = lookuphash(G->table, G->names[i]);
        G->table->array[idx]->value->color = colorlist[i];
    }
    free(colorlist);
    return colornum;
}


int Bipartito(Grafo G){
    queue q = createqueue(1400);
    u32 s = 0;
    u32 idx = 0;
    u32 maxint = UINT32_MAX;
    neighbor n, m; 

    q = enqueue(q, G->names[0]);
    idx = lookuphash(G->table, G->names[0]);
    G->table->array[idx]->value->color = 0;
    while (is_empty(q) != 0) {
        s = front(q);
        q = dequeue(q);

        idx = lookuphash(G->table, s);
        n = G->table->array[idx]->value;
        for (u32 i = 0; i < n->length; i++) {
            idx = lookuphash(G->table, n->list[i]);
            m = G->table->array[idx]->value;

            if (n->color != m->color) {
                if (m->color == maxint) {
                    m->color = 1 - n->color;
                    q = enqueue(q, n->list[i]);
                }
            } else {
                freequeue(q);
                return -1;
            }
        }
    }
    freequeue(q);
    return 0;
}

u32 NumeroDeVertices(Grafo G) {
    return G->vertex;
}

u32 NumeroDeLados(Grafo G) {
    return G->edges;
}

u32 NumeroDeColores(Grafo G) {
    return G->color;
}

u32 NombreDelVertice(Grafo G, u32 i) {
    return G->names[i];
}

u32 ColorDelVertice(Grafo G, u32 i) {
    u32 idx = lookuphash(G->table, i);
    return G->table->array[idx]->value->color;
}

u32 GradoDelVertice(Grafo G, u32 i) {
    u32 idx = lookuphash(G->table, i);
    return G->table->array[idx]->value->length;
}

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j) {
    u32 vecino = NombreJotaesimoVecino(G, i, j);
    u32 idx = lookuphash(G->table, vecino);
    return G->table->array[idx]->value->color;
}

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j) {
    u32 idx = lookuphash(G->table, G->names[i]);
    return G->table->array[idx]->value->list[j];
}

u32 sonadjacentes(Grafo G, u32 i, u32 j) {
    u32 vertice1 = NombreDelVertice(G, i);
    u32 vertice2 = NombreDelVertice(G, j);
    u32 idx = lookuphash(G->table, vertice1);

    neighbor newneighbor = G->table->array[idx]->value;
    for (u32 i = 0; i < newneighbor->length; i++) {
        if (newneighbor->list[i] == vertice2) {
            return 1;
        }
    }
    return 0;
}
