#include "htable.h"

item createitem(u32 key) {
    item newitem = malloc(sizeof(struct _hash_t));
    newitem->key = key;

    neighbor newneighbor = malloc(sizeof(struct _neighbor_t));
    newneighbor->color = UINT32_MAX;
    newneighbor->length = 0;
    newneighbor->list = NULL;
    newitem->value = newneighbor;

    return newitem;
}

void deleteitem(item i) {
    if (i != NULL) {
        if (i->value->list != NULL) {
            free(i->value->list);
        }
        free(i->value);
        free(i);
    }
    return;
}

item copyitem(item i) {
    item icopy = malloc (sizeof(struct _hash_t));
    *icopy = *i;

    neighbor ncopy = malloc(sizeof(struct _neighbor_t));
    *ncopy = *(i->value);

    u32* neighborlist = calloc(i->value->length, sizeof(u32));
    neighborlist = memcpy(neighborlist, i->value->list, i->value->length * sizeof(u32));
    ncopy->list = neighborlist;

    icopy->value = ncopy;
    return icopy;
}

hashtable createhash(u32 size) {
    hashtable newhash = calloc(1, sizeof(struct _hashtable_t));

    newhash->count = 0;
    newhash->size = size;
    newhash->array = calloc(size, sizeof(item));
    return newhash;
}

u32 hashfunc(u32 key, u32 size) {
   return key % size;
}

u32 inserttohash(hashtable table, u32 key) {
    u32 i = 0;

    item newitem = createitem(key);
    i = hashfunc(key, table->size);
    while (table->array[i] != NULL) {
        i = hashfunc(i+1, table->size);
    }
    table->array[i] = newitem;
    table->count++;

    return i;
}


void deletehash(hashtable table) {
    if (table != NULL) {
        for (u32 i = 0; i < table->size; i++) {
            deleteitem(table->array[i]);
        }
        free(table->array);
        free(table);
    }
    return;
}

u32 lookuphash(hashtable table, u32 key) {
    u32 result = UINT32_MAX;
    u32 i = hashfunc(key, table->size);

    while (table->array[i] != NULL) {
        
        if (table->array[i]->key == key) {
            result = i;
            break;
        }
        i = hashfunc(i+1, table->size);
    }
    return result;
}

u32 removefromhash(hashtable table, u32 key) {
    u32 i = 0;

    i = lookuphash(table, key);
    if (i != UINT32_MAX) {
        deleteitem(table->array[i]);
        table->array[i] = NULL;
        table->count--;
    }
    return 0;
}

hashtable copyhash(hashtable table) {
    hashtable hcopy = malloc(sizeof(struct _hashtable_t));
    *hcopy = *table;

    item* itemscopy = calloc(table->size, sizeof(item));
    for (u32 i = 0; i < table->size; i++) {
        if (table->array[i] != NULL) {
            itemscopy[i] = copyitem(table->array[i]);
        }
    }
    hcopy->array = itemscopy;
    return hcopy;
}
