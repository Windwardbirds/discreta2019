#ifndef HTABLE_H
#define HTABLE_H
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* Unsigned de 32 bits. Requiere libreria stdint. */
typedef uint32_t u32;

typedef struct _neighbor_t {
    u32 color;
    u32 length;
    u32* list;
} *neighbor;

typedef struct _hash_t {
    u32 key;
    neighbor value;
} *item;

typedef struct _hashtable_t {
    u32 count; //amount of items currently in the hash table
    u32 size; //size of the hash table array
    item* array; //array of items
} *hashtable;

/* Funciones sobre items */
item createitem(u32 key);
void deleteitem(item i);

/* Funciones sobre una hashtable */
hashtable createhash(u32 size);
void deletehash(hashtable i);
u32 hashfunc(u32 key, u32 size);
u32 inserttohash(hashtable table, u32 key);
u32 lookuphash(hashtable table, u32 key);
u32 removefromhash(hashtable table, u32 key);
hashtable copyhash(hashtable table);

#endif
