#ifndef QUEUE_H
#define QUEUE_H
#include "Rii.h"

typedef struct _queue_t {
    int front;
    int rear;
    u32 capacity; 
    u32* array; 
} *queue;

queue createqueue(u32 capacity);

int is_empty(queue q);

u32 front(queue q);

queue enqueue(queue q, u32 n);

queue dequeue(queue q);

void freequeue(queue q);

#endif
