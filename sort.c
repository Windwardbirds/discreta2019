#include "sort.h"

int compare (const void * a, const void * b) {
  u32 fa = *(u32*) a;
  u32 fb = *(u32*) b;
  return (fa > fb) - (fa < fb);
}

char OrdenNatural(Grafo G) {
    qsort(G->names, G->table->count, sizeof(u32), compare);
    return 0;
}

int comparewp (const void * a, const void * b) {
  vertex fa = (vertex) a;
  vertex fb = (vertex) b;
  return (fb->grado - fa->grado);
}

char OrdenWelshPowell(Grafo G) {
    vertex *gradelist = calloc(G->table->count, sizeof(struct _vertex_t));
    u32 idx = 0;
    for (u32 i = 0; i < G->table->count; i++) {
        idx = lookuphash(G->table, i);
        gradelist[i]->nombre = G->names[i];
        gradelist[i]->grado = G->table->array[idx]->value->length;
    }
    qsort(gradelist, G->table->count, sizeof(struct _vertex_t), comparewp);
    free(gradelist);
    return 0;
}

/*
char RMBCnormal(Grafo G) {
    u32 idx, i;
    u32 listsize = 0;
    u32* RMBClist = NULL;

    hashtable G->colorhash;
    for (i = 0; i < G->colors; i++) {
        idx = lookuphash(G->colorhash, i);
        listsize = G->colorhash->array[idx]->value->length + listsize;
        RMBClist = (u32*)realloc(RMBClist, listsize * sizeof(u32));
        RMBClist = memcpy(RMBClist, G->colorhash->array[idx]->value->list, listsize * sizeof(u32));
    }
    free(G->names);
    G->names = RMBClist;
    return 0;
}*/

/*char RMBCrevierte(Grafo G) {
    u32 idx, i;
    u32 listsize = 0;
    u32* RMBClist = NULL;

    //hashtable G->colorhash;
    for (i = G->colors; i > 0; i--) {
        idx = lookuphash(G->colorhash, i);
        listsize = G->colorhash->array[idx]->value->length + listsize;
        RMBClist = (u32*)realloc(RMBClist, listsize * sizeof(u32));
        RMBClist = memcpy(RMBClist, G->colorhash->array[idx]->value->list, listsize * sizeof(u32));
    }
    free(G->names);
    G->names = RMBClist;
    return 0;
}*/

char RMBCchicogrande(Grafo G){
    u32 idx, i;
    u32 listsize = 0;
    u32 *RMBClist = NULL;
    u32 offset = 0;
    hashtable hashc = G->colorhash;
    neighbor n = NULL;

    for (i = 0; i < G->colorhash->count; i++) {
        idx = lookuphash(hashc, i);
        n = hashc->array[idx]->value;
        qsort(n->list, n->length, sizeof(u32), compare);
        listsize = n->length + listsize;
        RMBClist = (u32*)realloc(RMBClist, listsize * sizeof(u32));
        memcpy(&RMBClist[offset], n->list, n->length * sizeof(u32));
        offset = offset + n->length;
    }
    free(G->names);
    G->names = RMBClist;
    return 0;
}

char SwitchVertices(Grafo G,u32 i,u32 j) {
    u32 vertices = G->table->count;
    if ((i > vertices) && (j > vertices)) {
        return '1';
    }
    u32 temp = 0;
    temp = G->names[i];
    G->names[i] = G->names[j];
    G->names[j] = temp;

    return '0';
}
