Para compilar:

make
./parcialito

o bien

valgrind --leak-check=full --show-leak-kinds=all ./parcialito

Para borrar ejecutables y objetos:
make clean
