#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "helpers.h"

typedef uint32_t u32;

struct _neighbor_t {
    u32 length;
    u32* list;
};

typedef struct _neighbor_t *neighbor;
/*
struct GraphSt {
    u32 vertex;
    u32 edges;
    u32** listaadj;
};*/

struct GraphSt {
    u32 vertex;
    u32 edges;
    neighbor listaadj;
};

typedef struct GraphSt *Grafo;
Grafo add_neighbors(Grafo g);

Grafo add_neighbors(Grafo g, u32 i, u32 j) {
    u32 size = (g->listaadj)[index-1].length;
    u32 *vertexlist = (g->listaadj)[index-1].list;
    if (size == 0) {
        printf("Creating new list for this vertex's neighbors. :)\n");
        vertexlist = calloc(1, sizeof(u32));
        vertexlist[0] = value;
        (newgraph->listaadj)[index-1].list = vertexlist;
        (newgraph->listaadj)[index-1].length++;
    } else {
        vertexlist = (u32*)realloc((newgraph->listaadj)[index-1].list, sizeof(u32) * size);
        vertexlist[size] = value;
        (newgraph->listaadj)[index-1].length++;
        printf("Here's the rebuilt element %d\n", ((newgraph->listaadj)[index-1].list)[size]);
   }
    return g;
}

int main(void) {

    Grafo newgraph = NULL;
    newgraph = calloc(1, sizeof(struct GraphSt));

    char *word = NULL;
    char *line = NULL;
    char *def = NULL;
    char *def1 = NULL;
    u32 index = 0;
    u32 value = 0;
    FILE *fd = NULL;
    const string_t filename = string_create("sometext.txt");

    fd = fopen(string_ref(filename), "r");
    if (fd != NULL) {
        while (!feof(fd)) {
            line = readline(fd);
            if (line == NULL) {
                break;
            }
            word = strtok(line, " ");

            if (strcmp(word, "c") == 0) {
                printf("this line contains comments\n");
            } else {
                if (strcmp(word, "p") == 0) {
                    def = strtok(NULL, " ");
                    def1 = strtok(NULL, " ");
                    def = strtok(NULL, "\n");
                    //BUILD GRAPH STRUCTURE
                    printf("def1: %s\ndef2: %s\n\n", def, def1);
                    newgraph->vertex = (u32)strtol(def, NULL, 10);
                    newgraph->edges = (u32)strtol(def1, NULL, 10);
                    printf("OK I TOOK THIS ONE FROM THE GRAPH %d\n", newgraph->vertex);

                    neighbor adjacentes = calloc(newgraph->vertex+1, sizeof(struct _neighbor_t));
                    newgraph->listaadj = adjacentes;

                    for (index = 0; index < newgraph->vertex; index++) {
                        (newgraph->listaadj)[index].length = 0;
                        (newgraph->listaadj)[index].list = NULL;
                        printf("Inicializando la lista de adjacencia -.-\n");
                    }
                } else {
                    if (strcmp(word, "e") == 0) {
                        //BUILD EDGE
                        def = strtok(NULL, " ");
                        def1 = strtok(NULL, "\n");
                        index = (u32)strtol(def, NULL, 10);
                        value = (u32)strtol(def1, NULL, 10);
                        //struct _neighbor_t somename = newgraph->listaadj[index-1];

                        //u32 size = somename->length;
                        //u32 *vertexlist = somename->list;

                        u32 size = (newgraph->listaadj)[index-1].length;
                        u32 *vertexlist = (newgraph->listaadj)[index-1].list;
                        //(newgraph->listaadj)[index].list = NULL; //(u32*)realloc(trial, sizeof(u32) * 5);;
                        if (size == 0) {
                            printf("Creating new list for this vertex's neighbors. :)\n");
                            vertexlist = calloc(1, sizeof(u32));
                            vertexlist[0] = value;
                            (newgraph->listaadj)[index-1].list = vertexlist;
                            (newgraph->listaadj)[index-1].length++;
                        } else {
                            vertexlist = (u32*)realloc((newgraph->listaadj)[index-1].list, sizeof(u32) * size);
                            vertexlist[size] = value;
                            (newgraph->listaadj)[index-1].length++;
                            printf("Here's the rebuilt element %d\n", ((newgraph->listaadj)[index-1].list)[size]);
                        }

                        size = (newgraph->listaadj)[value-1].length;
                        vertexlist = (newgraph->listaadj)[value-1].list;
                        if (size == 0) {
                            printf("Creating new list for this vertex's neighbors. :)\n");
                            vertexlist = calloc(1, sizeof(u32));
                            vertexlist[0] = index;
                            (newgraph->listaadj)[value-1].list = vertexlist;
                            (newgraph->listaadj)[value-1].length++;
                        } else {
                            vertexlist = (u32*)realloc((newgraph->listaadj)[value-1].list, sizeof(u32) * size);
                            vertexlist[size] = index;
                            (newgraph->listaadj)[value-1].length++;
                            printf("Here's the rebuilt element %d\n", ((newgraph->listaadj)[value-1].list)[size]);
                        }
                        //*somename[index].list = (u32*)realloc(somename[index], sizeof(u32) * size);
                    }
                }
            }
            printf("this is my word: %s aaaaand %s, %s\n", word, def, def1);

            free(line);
            line = NULL;
        }
    }
    fclose(fd);

    for (index = 0; index < newgraph->vertex; index++) {
        printf("\n");
        for (u32 j = 0; j < (newgraph->listaadj)[index].length; j++) {
            printf("%d ", ((newgraph->listaadj)[index]).list[j]);
        }
    }
    printf("\n");
    return 0;
}
