#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "helpers.h"

typedef uint32_t u32;

struct _neighbor_t {
    u32 length;
    u32* list;
};
/*
struct GraphSt {
    u32 vertex;
    u32 edges;
    u32** listaadj;
};*/

struct GraphSt {
    u32 vertex;
    u32 edges;
    struct neighbor* listaadj;
};

typedef struct _neighbor_t *neighbor
typedef struct GraphSt *Grafo;

int main(void) {

    Grafo newgraph = NULL;
    newgraph = calloc(1, sizeof(struct GraphSt));

    char *word = NULL;
    char *line = NULL;
    char *def = NULL;
    char *def1 = NULL;
    u32 index = 0;
    u32 value = 0;
    FILE *fd = NULL;
    const string_t filename = string_create("sometext.txt"); //"sometext.txt";

    fd = fopen(string_ref(filename), "r");
    if (fd != NULL) {
        while (!feof(fd)) {
            line = readline(fd);
            if (line == NULL) {
                break;
            }
            word = strtok(line, " ");
            if (strcmp(word, "c") == 0) {
                printf("this line contains comments\n");
            } else {
                if (strcmp(word, "p") == 0) {
                    def = strtok(NULL, " ");
                    def1 = strtok(NULL, " ");
                    def = strtok(NULL, "\n");
                    //BUILD GRAPH STRUCTURE
                    printf("def1: %s\ndef2: %s\n\n", def, def1);
                    newgraph->vertex = (u32)strtol(def, NULL, 10);
                    newgraph->edges = (u32)strtol(def1, NULL, 10);
                    printf("OK I TOOK THIS ONE FROM THE GRAPH %d\n", newgraph->vertex);

                    struct neighbor adjacentes = calloc(newgraph->vertex+1, sizeof(struct _neighbor_t));
                    newgraph->listaadj = adjacentes;
                    for (index = 0; index < newgraph->vertex; index++) {
                        (newgraph->listaadj)[index].length = 0;
                        (newgraph->listaadj)[index].list = NULL;
                        printf("Inicializando la lista de adjacencia -.-\n");
                    }

                    (adjacentes[0]).length = 0;
                    (newgraph->listaadj)->length = 0;
                } else {
                    if (strcmp(word, "e") == 0) {
                        //BUILD EDGE
                        def = strtok(NULL, " ");
                        def1 = strtok(NULL, "\n");
                        index = (u32)strtol(def, NULL, 10);
                        value = (u32)strtol(def1, NULL, 10);
                        struct neighbor *somename = newgraph->listaadj[index-1];

                        u32 size = somename->length;
                        u32 *vertexlist = somename->list;
                        //u32 size = (newgraph->listaadj)[index].length;
                        //(newgraph->listaadj)[index].list = NULL; //(u32*)realloc(trial, sizeof(u32) * 5);;
                        if (size == 0) {
                            printf("Creating new list for this vertex's neighbors. :)\n");
                            vertexlist = calloc(1, sizeof(u32));
                            vertexlist[0] = value;
                            somename->list = vertexlist;
                            somename->length++;
                            printf("Here %d\n", (somename->length));
                        } else {
                            printf("Now look this way.\n");
                            vertexlist = somename->list;
                            vertexlist = (u32*)realloc(somename->list, sizeof(u32) * size);
                        }
                        //*somename[index].list = (u32*)realloc(somename[index], sizeof(u32) * size);
                    }
                }
            }
            printf("this is my word: %s aaaaand %s, %s\n", word, def, def1);
            free(line);
            line = NULL;
        }
    }
    fclose(fd);
/*
    u32 i = 0;
    u32 n = 10;
    u32 m = 5;
    printf("HERE I CREATE A GRAPH!!\n\n");
    Grafo newgraph = NULL;
    u32 **adjacentes = calloc(n+1, sizeof(u32 *));
    newgraph = calloc(1, sizeof(struct GraphSt));
    newgraph->vertex = n;
    newgraph->edges = m;
    newgraph->listaadj = adjacentes;
    for (i = 0; i < newgraph->vertex; i++) {
        printf("Estoy aqui para quererte y \n");
        newgraph->listaadj[i] = NULL;
    }
    //newgraph->listavertices = vertexlist;
*/
    return 0;
}
