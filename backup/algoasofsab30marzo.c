#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "helpers.h"

typedef uint32_t u32;

struct _neighbor_t {
    u32 length;
    u32* list;
};

typedef struct _neighbor_t *neighbor;

struct GraphSt {
    u32 vertex;
    u32 edges;
    neighbor listaadj;
};

typedef struct GraphSt *Grafo;

Grafo add_neighbors(Grafo g, u32 i, u32 j);

Grafo add_neighbors(Grafo g, u32 i, u32 j) {
    u32 size = (g->listaadj)[i-1].length;
    u32 *vertexlist = (g->listaadj)[i-1].list;
    if (size == 0) {
        printf("Creating new list for this vertex's neighbors. :)\n");
        vertexlist = calloc(1, sizeof(u32));
        vertexlist[0] = j;
        (g->listaadj)[i-1].list = vertexlist;
        (g->listaadj)[i-1].length++;
    } else {
        u32 *resizedlist = (u32*)realloc((g->listaadj)[i-1].list, sizeof(u32) * (size+1));
        resizedlist[size] = j;
        (g->listaadj)[i-1].list = resizedlist;
        (g->listaadj)[i-1].length++;
        printf("Here's the rebuilt element %d\n", ((g->listaadj)[i-1].list)[size]);
   }
    return g;
}

int main(void) {

    Grafo newgraph = NULL;
    newgraph = calloc(1, sizeof(struct GraphSt));

    char *word = NULL;
    char *line = NULL;
    char *def = NULL;
    char *def1 = NULL;
    u32 index = 0;
    u32 value = 0;
    u32 j = 0;
    FILE *fd = NULL;
    string_t filename = string_create("sometext.txt");

    fd = fopen(string_ref(filename), "r");

    string_destroy(filename);

    if (fd != NULL) {
        while (!feof(fd)) {
            line = readline(fd);
            if (line == NULL) {
                break;
            }
            word = strtok(line, " ");

            if (strcmp(word, "c") == 0) {
                printf("this line contains comments\n");
            } else {
                if (strcmp(word, "p") == 0) {
                    def = strtok(NULL, " ");
                    def1 = strtok(NULL, " ");
                    def = strtok(NULL, "\n");
                    //BUILD GRAPH STRUCTURE
                    newgraph->vertex = (u32)strtol(def, NULL, 10);
                    newgraph->edges = (u32)strtol(def1, NULL, 10);

                    neighbor adjacentes = calloc(newgraph->vertex+1, sizeof(struct _neighbor_t));
                    newgraph->listaadj = adjacentes;

                    for (index = 0; index < newgraph->vertex; index++) {
                        (newgraph->listaadj)[index].length = 0;
                        (newgraph->listaadj)[index].list = NULL;
                    }
                } else {
                    if (strcmp(word, "e") == 0) {
                        //BUILD EDGE
                        def = strtok(NULL, " ");
                        def1 = strtok(NULL, "\n");
                        index = (u32)strtol(def, NULL, 10);
                        value = (u32)strtol(def1, NULL, 10);
                        add_neighbors(newgraph, index, value);
                        add_neighbors(newgraph, value, index);
                    }
                }
            }
            printf("this is my word: %s aaaaand %s, %s\n", word, def, def1);
            free(line);
            line = NULL;
        }
    }
    fclose(fd);
    for (index = 0; index < newgraph->vertex; index++) {
        printf("\n");
        for (j = 0; j < (newgraph->listaadj)[index].length; j++) {
            printf("%d ", ((newgraph->listaadj)[index]).list[j]);
        }
    }
    printf("\n");

    for (index = 0; index < newgraph->vertex; index++) {
        if ((newgraph->listaadj)[index].length != 0) {
            free((newgraph->listaadj)[index].list);
            printf("DEleting one list\n");
        }
        free(newgraph->listaadj);
    }
    free(newgraph);
    printf("chauchiisss\n");
    return 0;
}
