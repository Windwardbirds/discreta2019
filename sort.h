#ifndef SORT_H
#define SORT_H
#include "Rii.h"

typedef struct _vertex_t{
    u32 nombre;
    u32 grado;
} *vertex;

int compare (const void * a, const void * b);

int comparewp (const void * a, const void * b);

char OrdenNatural(Grafo G);

char OrdenWelshPowell(Grafo G);

char RMBCchicogrande(Grafo G);

char SwitchVertices(Grafo G,u32 i,u32 j);

#endif
