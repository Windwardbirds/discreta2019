#ifndef RII_H
#define RII_H
#include <stdio.h>
#include <stdint.h>
#include "htable.h"
#include "queue.h"

typedef struct GraphSt {
    u32 vertex;
    u32 edges;
    u32 color;
    hashtable table;
    hashtable colorhash;
    u32* names;
} *Grafo;

//typedef struct GraphSt *Grafo;

/* Unsigned de 32 bits. Requiere libreria stdint. */
//typedef uint32_t u32;

/* Funciones de coloreo */

u32 Greedy(Grafo G);

int Bipartito(Grafo G);

/* Funciones para extraer informacion del grafo */

u32 NumeroDeVertices(Grafo G);

u32 NumeroDeLados(Grafo G) ;

u32 NumeroDeColores(Grafo G);

/* Funciones de vertices */

u32 NombreDelVertice(Grafo G, u32 i);

u32 ColorDelVertice(Grafo G, u32 i);

u32 GradoDelVertice(Grafo G, u32 i);

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j);

u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j);

u32 sonadjacentes(Grafo G, u32 i, u32 j);

#endif
