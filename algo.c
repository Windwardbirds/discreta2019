#include <string.h>
#include "helpers.h"
#include "htable.h"
#include "Rii.h"
#include "sort.h"

void add_neighbors(Grafo g, u32 i, u32 j);

void add_neighbors(Grafo g, u32 i, u32 j) {
    hashtable table = g->table;
    u32 idx = lookuphash(table, i);
    if (idx == UINT32_MAX) {
        g->names[table->count] = i;
        idx = inserttohash(table, i);
    }
    neighbor elem = table->array[idx]->value;
    elem->length++;
    u32 size = elem->length;
    u32 *vertexlist = elem->list;

    vertexlist = (u32*)realloc(vertexlist, size * sizeof(u32));
    vertexlist[size-1] = j;
    elem->list = vertexlist;

    return;
}


Grafo CopiarGrafo(Grafo G) {
    Grafo graphcopy = malloc(sizeof(struct GraphSt));
    u32* namelistcopy = calloc(G->table->count, sizeof(u32));

    *graphcopy = *G;
    namelistcopy = memcpy(namelistcopy, G->names, G->table->count * sizeof(u32));
    graphcopy->names = namelistcopy;
    graphcopy->table = copyhash(G->table);

    return graphcopy;
}

void DestruccionDelGrafo(Grafo G) {
    deletehash(G->table);
    deletehash(G->colorhash);
    free(G->names);
    free(G);

    return;
}

int main(void) {

    Grafo newgraph = NULL;
    newgraph = calloc(1, sizeof(struct GraphSt));
    newgraph->names = NULL;

    char *word = NULL;
    char *line = NULL;
    char *def = NULL;
    char *def1 = NULL;
    u32 index = 0;
    u32 value = 0;
    int loadfactor;
    FILE *fd = NULL;
    string_t filename = string_create("sometext.txt");

    fd = fopen(string_ref(filename), "r");

    string_destroy(filename);

    if (fd != NULL) {
        while (!feof(fd)) {
            line = readline(fd);
            if (line == NULL) {
                break;
            }
            word = strtok(line, " ");

            if (strcmp(word, "e") == 0) {
                //BUILD EDGE
                def = strtok(NULL, " ");
                def1 = strtok(NULL, "\n");
                index = (u32)strtol(def, NULL, 10);
                value = (u32)strtol(def1, NULL, 10);

                add_neighbors(newgraph, index, value);
                add_neighbors(newgraph, value, index);
            } else {
                if (strcmp(word, "c") == 0) {
                    printf("this line contains comments\n");
                } else {
                    if (strcmp(word, "p") == 0) {
                        def = strtok(NULL, " ");
                        def1 = strtok(NULL, " ");
                        def = strtok(NULL, "\n");
                        //BUILD GRAPH STRUCTURE
                        newgraph->vertex = (u32)strtol(def1, NULL, 10);
                        newgraph->edges = (u32)strtol(def, NULL, 10);
                        newgraph->color = 0;

                        loadfactor = (u32)((float)newgraph->vertex * 1.35);
                        printf("\nthis is your load factor %d", loadfactor);
                        hashtable newhash = createhash(loadfactor);
                        newgraph->table = newhash;

                        u32* newnames = calloc(newgraph->vertex, sizeof(u32));
                        newgraph->names = newnames;
                    }
                }
            }
            //printf("first value: %s, second value: %s\n", def, def1);
            free(line);
            line = NULL;
        }
    }
    fclose(fd);
    newgraph->names = (u32*)realloc(newgraph->names, newgraph->table->count * sizeof(u32));
/*    for (u32 counter = 0; counter < newgraph->table->count; counter++) {
        printf("variable en posicion %d : %u\n", counter, newgraph->names[counter]);
    }*/

    printf("Running greedy without ordering!\n");
    newgraph->color = Greedy(newgraph);
    printf("Results are in: This is the result of Greedy: %d!\n", newgraph->color);

    //OrdenNatural(newgraph);
    /*for (u32 counter = 0; counter < newgraph->table->count; counter++) {
        printf("variable en posicion %u : %u\n", counter, newgraph->names[counter]);
    }*/
    //u32 ithcolor, nthcolor;
    /*for (u32 counter = 0; counter < newgraph->table->count; counter++) {
        ithcolor = newgraph->names[counter];
        nthcolor = ColorDelVertice(newgraph, ithcolor);
        printf("variable en posicion %u : %u, color: %u\n", counter, ithcolor, nthcolor);
    }*/
    RMBCchicogrande(newgraph);
    if (newgraph->names == NULL) {
        printf("hahaha what a loser");
    } else {
        printf("lolwin");
    }
    u32 ithcolor, nthcolor;
    for (u32 counter = 0; counter < newgraph->table->count; counter++) {
        ithcolor = newgraph->names[counter];
        nthcolor = ColorDelVertice(newgraph, ithcolor);
        printf("variable en posicion %u : %u, color:%u\n", counter, ithcolor, nthcolor);
    }

    /*printf("Running greedy with new order!\n");
    result = Greedy(newgraph);
    printf("Results are in: This is the result of Greedy: %d!\n", result);
*/
/*    printf("Checkeando si es bipartito\n");
    result = (u32)Bipartito(newgraph);
    if (result == 0) {
        printf("Es bipartito!!!\n");
    } else {
        printf("No es bipartito ;_;\n");
    }
*/
/*    RMBCchicogrande(newgraph);
    for (u32 counter = 0; counter < newgraph->table->count; counter++) {
        ithcolor = newgraph->names[counter];
        nthcolor = ColorDelVertice(newgraph, ithcolor);
        printf("variable en posicion %u : %u, color: %u\n", counter, ithcolor, nthcolor);
    }*/

    //Grafo duplicate = CopiarGrafo(newgraph);
    DestruccionDelGrafo(newgraph);
    //DestruccionDelGrafo(duplicate);
    return 0;
}


