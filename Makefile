CC = gcc
CCFLAGS = -Wall -Werror -Wextra -pedantic -std=c99 -g
OBJECTS = string.o helpers.o htable.o Rii.o sort.o queue.o algo.o

.PHONY: clean test

all: parcialito
%.o : %.c
	$(CC) $(CCFLAGS) -c -o $@ $^

parcialito: algo.o string.o helpers.o htable.o Rii.o sort.o queue.o
	$(CC) $(CCFLAGS) -o $@ $^

test: parcialito
	valgrind --show-reachable=yes --leak-check=full ./parcialito

clean:
	rm -f parcialito $(OBJECTS)
