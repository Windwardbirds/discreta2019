#include "queue.h"

queue createqueue(u32 capacity) {
    queue newqueue = calloc(1, sizeof(struct _queue_t));
    newqueue->capacity = capacity;
    newqueue->front = -1;
    newqueue->rear = -1;
    u32* newarray = calloc(capacity, sizeof(u32));
    newqueue->array = newarray;
    return newqueue;
}

int is_empty(queue q) {
    if ((q->front == -1) && (q->rear == -1)) {
        return 0;
    }
    return -1;
}

u32 front(queue q) {
    return q->array[q->front];
}

queue enqueue(queue q, u32 n) {
    if (is_empty(q) == 0) {
        q->front = 0;
        q->rear = 0;
    } else {
        q->rear++;
    }
    q->array[q->rear] = n;
    return q;
}

queue dequeue(queue q) {
    if (is_empty(q) != 0) {
        if (q->front == q->rear) {
            q->front = -1;
            q->rear = -1;
        } else {
            q->front++;
        }
    }
    return q;
}

void freequeue(queue q) {
    free(q->array);
    free(q);
    return;
}
